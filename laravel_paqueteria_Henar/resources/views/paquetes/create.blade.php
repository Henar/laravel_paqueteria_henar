@extends('layouts.master')

@section('titulo')
    Paqueteria
@endsection

@section('contenido')
    
    @php
       
    @endphp
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                   Añadir nuevo paquete
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('paquetes.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="direccion">Introduzca la fdireccion de entrega del paquete</label><br>
                            
                            @error('direccion')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="date" name="direccion" id="direccion" class="form-control" value="{{old('direccion')}}" required>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <select name="transportista" id="transportista" class="form-control" required>
    
                                @foreach ($transportistas as $tra)
                                   
                                    <option value="{{old($tra->id)}}">{{$tra->nombre}}</option>
                                @endforeach
                            </select>
                            
                            
                            @error('transportista')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            @foreach ($transportistas as $t)
                                
                            @endforeach
                            
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="imagen">Introduzca la foto</label><br>
                             
                            @error('imagen')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <input type="file" name="imagen" id="imagen"/>
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">Añadir paquete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection