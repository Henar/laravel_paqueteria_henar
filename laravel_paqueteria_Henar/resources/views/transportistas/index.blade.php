@extends('layouts.master')

@section('titulo')
    Transportistas
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <div class="row">
            
            @foreach($transportistas as $transportista)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <a href="{{ route('transportistas.show', $transportista)}}">                 
                        <h4 style="min-height:45px;margin:5px 0 10px 0">{{$transportista->nombre}}</h4>
                        <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" style="height:225px;margin-bottom:20px;padding:7px;"
                            class="rounded border border-3"/>
                        
                    </a>
                </div>
            @endforeach
        </div>
        <!--/*$animales->links()*/-->
    </form>
    
@endsection