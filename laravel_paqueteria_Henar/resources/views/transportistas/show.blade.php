@extends('layouts.master')

@section('titulo')
    Tranporstistas
@endsection

@section('contenido')
    
    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
    @php
        $fechaActual=date("Y-m-d");
        $añosCarne=floor(abs(strtotime($fechaActual)-strtotime($transportista->fechaPermisoConducir))/(365*60*60*24));
        $empresas=$transportista->empresas;
        $paquetes=$transportista->paquetes;
    @endphp 
    <form method="POST">
        @csrf
        <h1 style="margin-left: 25%">{{$transportista->nombre}} {{$transportista->apellidos}}</h1>
        <div class="row">
            <div class="col-sm-3">
                <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" style="height:25%"/>
            </div>
            <div class="col-sm-9">
                <h3>Años del permiso de circulación:</h3>
                <p>{{$añosCarne}}</p>
                <h3>Empresas:</h3>
                @foreach ($empresas as $empresa)
                    <p>{{$empresa->nombre}}</p>
                @endforeach
               
                <h3>Paquetes:</h3>
                
                @foreach ($paquetes as $paquete)
                    <p>{{$paquete->id}} - {{$paquete->direccion}}: 
                    @if ($paquete->entregado)
                        <b>pendiente de entrega</b>
                    @else
                        <b>entregado</b>
                    @endif
                    </p>
                @endforeach

                <br/>
                <a class="btn btn-warning" name="editar" style="margin-right: 7px" href = '{{ route('transportistas.entregar', $transportista)}}'>Entregar todo</a>
                <a class="btn btn-success" name="revision" style="margin-right: 7px" href = '{{ route('transportistas.noentregar', $transportista)}}'>Marcar como no entregado</a>
               
            </div>
        </div>
    </form>
@endsection