<?php

use App\Http\Controllers\PaqueteController;
use App\Http\Controllers\TransportistaController;
use App\Models\Reserva;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action([TransportistaController::class, 'index']);
});

Route::get('tranportistas', [TransportistaController::class, 'index'])->name('transportistas.index');

Route::get('tranportistas/{transportista}', [TransportistaController::class, 'show'])->name('transportistas.show');

Route::put('tranportistas/{transportistas}/entregar', [TransportistaController::class, 'entregar'])->name('transportistas.entregar');


Route::put('tranportistas/{transportista}/noentregado', [TransportistaController::class, 'noentregar'])->name('transportistas.noentregar');


Route::post('paquetes', [PaqueteController::class, 'store'])->name('paquetes.store');

Route::get('paquetes/crear', [PaqueteController::class, 'create'])->name('paquetes.create');


