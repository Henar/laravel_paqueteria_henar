<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use HasFactory;
    protected $table='paquetes';

    protected $fillable= ['imagen', 'direccion', 'transportista_id'];

    public function transportista(){
        return $this->belongsTo(Transportista::class);
    }

}
