<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    use HasFactory;
    protected $table='transportistas';


    public function getRouteKeyName()
    {
        return 'slug';
    }

    //Relacion uno a muchos
    public function paquetes(){
        return $this->hasMany(Paquete::class);
    }

    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }
}
