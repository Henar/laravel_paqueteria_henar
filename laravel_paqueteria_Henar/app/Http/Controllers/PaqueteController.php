<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    public function create(){
        $transportistas=Transportista::all();
        return view('paquetes.create', compact('transportistas'));
    }

public function store (Request $request){
        
        $request->validate([
            'direccion'=>'required|max:255',
            'transpor'=>'required|numeric|min:1|max:999999',
            
        ]);

        $transpor=Transportista::findOrFail($request->transportista);
        $paq=new Paquete();
        $paq->transportista_id=$transpor->id;
        $paq->direccion=$request->direccion;
        $paq->imagen=$request->foto->store('', 'imagenes');

        $paq->save();
        return redirect()->route('animales.show', $paq)->with('mensaje', "Paquete añadido con éxito");

    }
}
