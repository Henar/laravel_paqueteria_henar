<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Database\Seeders\TransportistaSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransportistaController extends Controller
{
    public function index(){
        $transportistas=Transportista::all();
        return view('transportistas.index', compact('transportistas'));

    }

    public function show(Transportista $transportista){
        $trans=Transportista::findOrFail($transportista->id);
        return view('transportistas.show', ['transportista'=>$trans]);
    }

    public function entregar(Transportista $t){
        $trans=Transportista::findOrFail($t->id);
        $paquetes=DB::table('paquetes')->where('transportista_id', $trans->id)->get();
        
        return redirect()->route('transpotistas.show', $trans)->with('mensaje', "Se han entregado todos los paquetes");
    }

    public function noentregar(Transportista $t){
        $trans=Transportista::findOrFail($t->id);
        $paquetes=DB::table('paquetes')->where('transportista_id', $trans->id)->get();
        return redirect()->route('transpotistas.show', $trans)->with('mensaje', "Se han marcado todos los paquetes como no entregado");

    }
}
